package fr.inria.integraal.view_parser.parser;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This class represents the whole document in the mapping specification format
 * @author Florent Tornil
 */
public class ViewDocument {
	
	private final Collection<DatasourceDescription> datasources;
	private final Collection<ViewDefinition> mappings;
	
	/**
	 * Create a new document with the given parameters
	 * @param datasources the datasources of the document
	 * @param mappings the mappings of the document
	 */
	public ViewDocument(Collection<DatasourceDescription> datasources, Collection<ViewDefinition> mappings) {
		this.datasources = datasources;
		this.mappings = mappings;
	}

	//
	// Datasource operations
	//

	/**
	 * Getter for datasources of the document
	 * @return all the datasources registered in this object
	 */
	public Collection<DatasourceDescription> getDatasources() {
		return this.datasources;
	}
	
	/**
	 * Getter for datasource of the document by id
	 * @param id the id of a datasource
	 * @return the datasource with the given id. An empty optional is returned is no datasource with this id is registered in this object
	 */
	public Optional<DatasourceDescription> getDatasourceById(String id) {
		return this.getDatasources().parallelStream().filter(ds -> ds.getId().equals(id)).findAny();
	}
	
	//
	// Mapping operations
	//
	
	/**
	 * Getter for views of the document
	 * @return all the mappings registered in this object
	 */
	public Collection<ViewDefinition> getMappings() {
		return this.mappings;
	}
	
	/**
	 * Getter for views of the document by datasource
	 * @param ds the description of a datasource
	 * @return all the mappings registered in this object with the given datasource
	 */
	public Collection<ViewDefinition> getMappingsByDatasource(DatasourceDescription ds) {
		return this.getMappings().parallelStream().filter(m -> m.getDatasourceId().equals(ds.getId())).collect(Collectors.toList());
	}
	
	/**
	 * Getter for views of the document by id
	 * @param id the id of a mapping
	 * @return the mapping with the given id. An empty optional is returned is no mapping with this id is registered in this object
	 */
	public Optional<ViewDefinition> getMappingById(String id) {
		return this.getMappings().parallelStream().filter(m -> m.getId().equals(id)).findAny();
	}
	
	//
	//
	//
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[Mapping Document]\n");
		sb.append("Datasources :");
		for(DatasourceDescription ds : this.getDatasources()) {
			sb.append("\n");
			sb.append("   ");
			sb.append(ds);
		}
		sb.append("\n\n");
		
		sb.append("Views :");
		for(ViewDefinition m : this.getMappings()) {
			sb.append("\n");
			sb.append("   ");
			sb.append(m);
		}
		sb.append("\n");
		
		return sb.toString();
	}

}
