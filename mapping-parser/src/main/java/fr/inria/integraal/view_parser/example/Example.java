package fr.inria.integraal.view_parser.example;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import fr.inria.integraal.view_parser.parser.ViewDocument;
import fr.inria.integraal.view_parser.parser.ViewParseException;
import fr.inria.integraal.view_parser.parser.ViewParser;

/**
 * Example of a vd file parsing
 * 
 * @author Florent Tornil
 *
 */
public class Example {

	/**
	 * Parse the given vd file and prints the parsed objects
	 * 
	 * @param args arguments of the program
	 */
	public static void main(String[] args) {

		if(args.length != 1) {
			System.out.println("Usage : java -jar vd-parser-jar-with-dependencies.jar vd_file_path");
			return;
		}

		String filePath = args[0];
		System.out.println("Loading file " + filePath);
		try {
			ViewDocument document = ViewParser.parse(filePath);
			System.out.println(document);
		} catch (FileNotFoundException e) {
			System.err.println("The given file " + filePath + " does not exist");
		} catch (IOException e) {
			System.err.println("An IO Exception occurred");
			System.err.println(e);
		} catch (ViewParseException e) {
			System.err.println("Something went wrong while parsing the file");
			System.err.println(e);
		} catch (URISyntaxException e) {
			System.err.println("An error occurred while trying to get the json schema file");
		}

	}

}
