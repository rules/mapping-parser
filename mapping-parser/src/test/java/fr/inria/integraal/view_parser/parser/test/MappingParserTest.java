package fr.inria.integraal.view_parser.parser.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import fr.inria.integraal.view_parser.parser.DatasourceDescription;
import fr.inria.integraal.view_parser.parser.ViewDefinition;
import fr.inria.integraal.view_parser.parser.ViewDocument;
import fr.inria.integraal.view_parser.parser.ViewParseException;
import fr.inria.integraal.view_parser.parser.ViewParser;

class MappingParserTest {

	@Test
	public void parseNormalMappingFileTest() throws Exception {
		String filepath = "src/test/resources/example_view.vd";
		ViewDocument document = ViewParser.parse(filepath);

		// Check datasources
		Collection<DatasourceDescription> datasources = document.getDatasources();
		assertEquals(3, datasources.size());

		assertTrue(document.getDatasourceById("POSTGRES_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("MYSQL_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("WEBAPI_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("Non_EXISTENT_SOURCE").isEmpty());

		// check mappings
		Collection<ViewDefinition> mappings = document.getMappings();
		assertEquals(3, mappings.size());

		assertTrue(document.getMappingById("vPgrsPerson").isPresent());
		assertTrue(document.getMappingById("vPgrsPerson2").isPresent());
		assertTrue(document.getMappingById("vApi").isPresent());
		assertTrue(document.getMappingById("vNonExistentMapping").isEmpty());

		assertEquals(2, document.getMappingsByDatasource(
				document.getDatasourceById("POSTGRES_SOURCE").get())
				.size());
		assertEquals(1, document.getMappingsByDatasource(
				document.getDatasourceById("WEBAPI_SOURCE").get())
				.size());
		assertEquals(0, document.getMappingsByDatasource(
				document.getDatasourceById("MYSQL_SOURCE").get())
				.size());
		
		assertEquals("SELECT fin, lan, gender, age FROM p WHERE fin=firstanme AND lan=lastname;\n", document.getMappingById("vPgrsPerson").get().getNativeQuery());
		assertEquals("SELECT lastname, firstname, gender, age FROM p;\n", document.getMappingById("vPgrsPerson2").get().getNativeQuery());
		
		assertEquals("postgres", document.getDatasourceById("POSTGRES_SOURCE").get().getParameters().get("password"));
	}

	//
	// Warning cases
	//

	@Test
	public void parseUnknownSourceTypeMappingFileTest() throws Exception {
		String filepath = "src/test/resources/mappingWithUnknownSourceType.json";
		ViewDocument document = ViewParser.parse(filepath);

		Collection<DatasourceDescription> datasources = document.getDatasources();
		assertEquals(3, datasources.size());

		assertTrue(document.getDatasourceById("SPECIAL_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("MYSQL_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("WEBAPI_SOURCE").isPresent());
		assertTrue(document.getDatasourceById("NON_EXISTENT_SOURCE").isEmpty());

		assertTrue(document.getDatasourceById("SPECIAL_SOURCE").isPresent());
	}

	@Test
	public void parseMappingDatasourceMissingRequiredParameterTest() throws Exception {
		String filepath = "src/test/resources/mappingDatasourceMissingRequiredParameter.json";
		ViewDocument document = ViewParser.parse(filepath);

		Collection<DatasourceDescription> datasources = document.getDatasources();
		assertEquals(3, datasources.size());
	}


	//
	// Fail cases
	//

	@Test
	public void parseMappingFileWithoutDatasourcesTest() {
		String filepath = "src/test/resources/missingDatasources.json";
		Throwable exception = assertThrows(ViewParseException.class, () -> ViewParser.parse(filepath));
		assertTrue(exception.getMessage().startsWith("This mapping file does not validate the Json Schema"));
	}

	@Test
	public void parseMappingFileWithoutMappingsTest() {
		String filepath = "src/test/resources/missingMappings.json";
		Throwable exception = assertThrows(ViewParseException.class, () -> ViewParser.parse(filepath));
		assertTrue(exception.getMessage().startsWith("This mapping file does not validate the Json Schema"));
	}

	@Test
	public void parseMappingWithoutItsDatasourceTest() {
		String filepath = "src/test/resources/mappingMissingItsDatasource.json";
		Throwable exception = assertThrows(ViewParseException.class, () -> ViewParser.parse(filepath));
		assertEquals("Found view vPgrsPerson but no datasource with id NON_EXISTENT_SOURCE exists", exception.getMessage());
	}

	@Test
	public void parseMappingWithMultipleDatasourceIdTest() {
		String filepath = "src/test/resources/mappingWithMultipleDatasourceId.json";
		Throwable exception = assertThrows(ViewParseException.class, () -> ViewParser.parse(filepath));
		assertEquals("Found two datasources with the same id : POSTGRES_SOURCE", exception.getMessage());
	}

	@Test
	public void parseMappingWithMultipleMappingIdTest() {
		String filepath = "src/test/resources/mappingWithMultipleMappingId.json";
		Throwable exception = assertThrows(ViewParseException.class, () -> ViewParser.parse(filepath));
		assertEquals("Found two views with the same id : vPgrsPerson", exception.getMessage());
	}

}
