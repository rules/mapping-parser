/**
 * Main module of the mapping parser
 * @author Florent Tornil
 */
module fr.inria.integraal.view_parser.parser {
	requires java.json;
	requires com.networknt.schema;
	requires com.fasterxml.jackson.databind;
    requires org.slf4j;
	requires java.desktop;

	exports fr.inria.integraal.view_parser.parser;
	
}