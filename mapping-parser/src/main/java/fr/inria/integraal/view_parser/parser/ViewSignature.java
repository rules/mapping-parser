package fr.inria.integraal.view_parser.parser;

import java.util.Optional;

/**
 * This class represents a template of the mapping specification format
 * @author Florent Tornil
 */
public class ViewSignature {



	private final Optional<String> mandatory;
	private final String ifMissing;
	private final Optional<String> selection;

	/**
	 * Create a new template with the given parameters
	 * @param mandatory the mandatory replacement key
	 * @param ifMissing the missing value handling strategy
	 * @param selection the JsonPath selection query in case of a JSON datasource
	 */
	public ViewSignature(String mandatory, String ifMissing, String selection) {
		this.mandatory = Optional.ofNullable(mandatory);
		this.ifMissing = ifMissing;
		this.selection = Optional.ofNullable(selection);
	}

	/**
	 * Getter for the view signature element mandatory
	 * @return the value of the mandatory field
	 */
	public Optional<String> getMandatory() {
		return this.mandatory;
	}

	/**
	 * Getter for the view signature element missing strategy
	 * @return the handling of missing value strategy
	 */
	public String getIfMissing() {
		return this.ifMissing;
	}

	/**
	 * Getter for the view signature element selection JsonPath query
	 * @return the JsonPath query for selection a value
	 */
	public Optional<String> getSelection() {
		return this.selection;
	}

	//
	//
	//

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(this.getMandatory().isPresent()) {
			sb.append("Mandatory as \"").append(this.getMandatory().get()).append("\" -- ");
		}
		if(this.getSelection().isPresent()) {
			sb.append("Select value with \"").append(this.getSelection().get()).append("\" -- ");
		}
		sb.append("If value is missing : ").append(this.getIfMissing());
		return sb.toString();
	}

}
