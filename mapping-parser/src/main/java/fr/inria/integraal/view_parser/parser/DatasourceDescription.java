package fr.inria.integraal.view_parser.parser;

import java.util.Map;
import java.util.Map.Entry;

/**
 * This class represents a datasource of the mapping specification format
 * @author Florent Tornil
 */
public class DatasourceDescription {

	private final String id;
	private final String type;

	private final Map<String, String> parameters;

	/**
	 * Create a new datasource with the given parameters
	 * @param id the id of the datasource
	 * @param type the type of the datasource
	 * @param parameters the parameters of the datasource
	 */
	public DatasourceDescription(String id, String type, Map<String, String> parameters) {
		this.id = id;
		this.type = type;
		this.parameters = parameters;
	}

	/**
	 * Getter for datasource id
	 * @return the id of this datasource
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Getter for datasource type
	 * @return the type of this datasource
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Getter for datasource parameters
	 * @return the map of parameters associated to this datasource
	 */
	public Map<String, String> getParameters() {
		return this.parameters;
	}
	
	//
	//
	//
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[Datasource Description]\n");
		sb.append("      Id : ").append(this.getId()).append("\n");
		sb.append("      Protocol : ").append(this.getType()).append("\n");
		sb.append("      Parameters : [");
		for(Entry<String, String> entry : this.getParameters().entrySet()) {
			sb.append("\n");
			sb.append("         ");
			sb.append(entry.getKey()).append(" = ").append(entry.getValue());
		}
		sb.append("\n      ]");
		return sb.toString();
	}

}
