# Mapping Parser

This project defines the mapping parser which serves as the basis for parsing mapping files formatted according to the specification.

The entry point of this parser is the `MappingParser` class and more precisely its `parse` method.

## Installation

The current release of MappingParser is version 1.0.0, corresponding to the VD format version 1.0.
See [the InteGraal website](https://rules.gitlabpages.inria.fr/integraal-website/features/vd) for more information about this format.

This MappingParser has been tested with Java version >= 11 up to the last java 21 LTS.

This library is available with Maven directly, by adding the following dependency to your `pom.xml` file:
```xml
<dependency>
    <groupId>fr.lirmm.graphik</groupId>
    <artifactId>mapping-parser</artifactId>
    <version>1.0.0</version>
</dependency>
```

To clone this repository, you can use
```sh
git clone git@gitlab.inria.fr:rules/mapping-parser.git
```

## Example Mapping Parser Application

As a basic example, below is a simple Mapping Parser that using the MappingParser class to print the datasources and mapping present in the file while handling the various exceptions that can be thrown by the parser:

```java
public class Example {

	public static void main(String[] args) {

		String filePath = args[0];
		System.out.println("Loading file " + filePath);

		try {
			MappingDocument document = MappingParser.parse(filePath);
			System.out.println(document);

		} catch (FileNotFoundException e) {
			System.err.println("The given file " + filePath + " does not exist");
		} catch (IOException e) {
			System.err.println("An IO Exception occured");
			System.err.println(e);
		} catch (MappingParseException e) {
			System.err.println("Something went wrong while parsing the file");
			System.err.println(e);
		} catch (URISyntaxException e) {
			System.err.println("An error occured while trying to get the json schema file");
		}
	}
}
```

This `Example` class is also available in this project and is the one used when executing the [provided executable jar file](https://gitlab.inria.fr/api/v4/projects/40997/jobs/artifacts/master/raw/mapping-parser/target/vd-parser-jar-with-dependencies.jar?job=mapping-parser-build).

## MappingDocument Methods

The result of the parser is a `MappingDocument` object that represents your Mapping file at an object level. It's methods are getters over the mapping and datasource fields as well as some helper methods the retrieve: 
- datasources by giving an ID,
- mappings by giving an ID or a datasource ID.

Please refer to the javadoc for more information.

## Contact

If you need more information about the library or encounter any problem, please contact [Florent Tornil](mailto:florent.tornil@inria.fr) or [Federico Ulliana](mailto:federico.ulliana@inria.fr).

If you encounter some unexpected behaviour, feel free to fill an issue on this Git an we will try our best to help you.
