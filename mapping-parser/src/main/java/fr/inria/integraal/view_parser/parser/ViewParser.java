package fr.inria.integraal.view_parser.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.*;
import javax.swing.text.View;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * This class represents the entry point of the mapping parser
 *
 * @author Florent Tornil
 */
public class ViewParser {

	private static final Logger LOG = LoggerFactory.getLogger(ViewParser.class);

	private static final String schemaPath = "/vd_schema.json";

	private static final List<String> IF_MISSING_SUPPORTED_VALUES = List.of("FREEZE", "EXIST", "IGNORE");
	private static final String IF_MISSING_DEFAULT_VALUE = "FREEZE";

	/**
	 * Create a new Mapping parser
	 */
	public ViewParser() {}

	/**
	 * Parse the document located at the given file path
	 * @param configFilePath the path of the configuration file to parse
	 * @return a MappingDocument containing all the information from the given file
	 * @throws IOException iff the file does not exist or another error occur
	 * @throws ViewParseException iff the document was semantically incorrect
	 * @throws URISyntaxException iff the json schema file is missing
	 */
	public static ViewDocument parse(String configFilePath) throws IOException, ViewParseException, URISyntaxException {
		JsonReader reader = Json.createReader(new FileReader(configFilePath));

		JsonObject document = reader.readObject();
		reader.close();

		JsonSchema schema = getJsonSchemaFromUrl(schemaPath);
		JsonNode node = getJsonNodeFromFilePath(configFilePath);
		Set<ValidationMessage> errors = schema.validate(node);
		if(!errors.isEmpty()) {
			throw new ViewParseException("This mapping file does not validate the Json Schema : " + errors.iterator().next().toString());
		}

		JsonArray JsonDatasources = document.getJsonArray(SchemaConstants.DATASOURCES_KEY.getLabel());
		Collection<DatasourceDescription> datasources = ViewParser.parseDatasources(JsonDatasources);

		JsonArray JsonMappings = document.getJsonArray(SchemaConstants.VIEWS_KEY.getLabel());
		Collection<ViewDefinition> mappings = ViewParser.parseMappings(JsonMappings);

		for(ViewDefinition mapping : mappings) {
			boolean existsDatasource = datasources.stream().anyMatch(d -> d.getId().equals(mapping.getDatasourceId()));
			if(!existsDatasource) {
				throw new ViewParseException("Found view " + mapping.getId() + " but no datasource with id " + mapping.getDatasourceId() + " exists");
			}
		}

		return new ViewDocument(datasources, mappings);
	}

	/**
	 * Parse the datasources array
	 * @param jsonDatasources datasources key entry
	 * @return the datasources descriptions
	 * @throws ViewParseException iff an error occur
	 */
	private static Collection<DatasourceDescription> parseDatasources(JsonArray jsonDatasources) throws ViewParseException {

		if(jsonDatasources == null) {
			throw new ViewParseException("No datasources found in the document");
		}

		Set<String> ids = new HashSet<>();
		List<DatasourceDescription> datasources = new ArrayList<>();

		for(JsonValue value : jsonDatasources) {
			JsonObject jsonDatasource = value.asJsonObject();

			// Check if the ids are unique
			String id = jsonDatasource.getString(SchemaConstants.DATASOURCE_ID_KEY.getLabel());
			boolean unique = ids.add(id);
			if(!unique) {
				throw new ViewParseException("Found two datasources with the same id : " + id);
			}

			String type = jsonDatasource.getString(SchemaConstants.DATASOURCE_PROTOCOL_KEY.getLabel());
			Optional<DatasourceType> datasourceTypeOpt = DatasourceType.getTypeByLabel(type);
			boolean checkConsistencyWithDatasourceType = datasourceTypeOpt.isPresent();
			if(!checkConsistencyWithDatasourceType) {
				LOG.warn("The protocol {} is not part of the predefined ones.", type);
			}

			// Create parameters map
			JsonObject jsonParameters = jsonDatasource.getJsonObject(SchemaConstants.DATASOURCE_PARAMETERS_KEY.getLabel());
			Map<String, String> parameters = new HashMap<>();
			if(jsonParameters != null) {
				for (String key : jsonParameters.keySet()) {
					String keyValue = jsonParameters.getString(key);
					if (key.equals(SchemaConstants.DATASOURCE_PARAMETERS_PASSWORD_FILE_KEY.getLabel())) {
						try {
							keyValue = Files.readString(Path.of(keyValue));
							parameters.put(SchemaConstants.DATASOURCE_PARAMETERS_PASSWORD_KEY.getLabel(), keyValue);
						} catch (IOException e) {
							throw new ViewParseException("An exception occurred while reading from the file " + keyValue + " for the datasource " + id + "\n" + e);
						}
						if (jsonParameters.containsKey(SchemaConstants.DATASOURCE_PARAMETERS_PASSWORD_KEY.getLabel())) {
							throw new ViewParseException("Found datasource " + id + " that declares both a password and a password file");
						}
					} else {
						parameters.put(key, keyValue);
					}
				}
			}

			if(checkConsistencyWithDatasourceType) {
				List<String> requiredParameters = datasourceTypeOpt.get().getRequiredParameters();
				for(String requiredParameter : requiredParameters) {
					if(! parameters.containsKey(requiredParameter)) {
						LOG.warn("The datasource {} is missing the required parameter {}.", id, requiredParameter);
					}
				}
			}

			datasources.add(new DatasourceDescription(id, type, parameters));
		}
		return datasources;
	}

	/**
	 * Parse the mappings array
	 * @param jsonMappings views key entry
	 * @return the mappings descriptions
	 * @throws ViewParseException iff an error occur
	 */
	private static Collection<ViewDefinition> parseMappings(JsonArray jsonMappings) throws ViewParseException {

		if(jsonMappings == null) {
			throw new ViewParseException("No views found in the document");
		}

		Set<String> ids = new HashSet<>();
		List<ViewDefinition> mappings = new ArrayList<>();

		for(JsonValue value : jsonMappings) {
			JsonObject jsonMapping = value.asJsonObject();

			// Check if the ids are unique
			String id = jsonMapping.getString(SchemaConstants.VIEW_ID_KEY.getLabel());
			boolean unique = ids.add(id);
			if(!unique) {
				throw new ViewParseException("Found two views with the same id : " + id);
			}

			String datasourceId = jsonMapping.getString(SchemaConstants.VIEW_DATASOURCE_KEY.getLabel());

			String nativeQuery;
			if(jsonMapping.containsKey(SchemaConstants.VIEW_QUERY_FILE_KEY.getLabel())) {
				String queryFile = jsonMapping.getString(SchemaConstants.VIEW_QUERY_FILE_KEY.getLabel());
				try {
					nativeQuery = Files.readString(Path.of(queryFile));
				} catch (IOException e) {
					throw new ViewParseException("An exception occurred while reading from the file " + queryFile + " for the query of view " + id + "\n" + e);
				}
				if(jsonMapping.containsKey(SchemaConstants.VIEW_QUERY_KEY.getLabel())) {
					throw new ViewParseException("Found view " + id + " that declares both a query and a query file");
				}
			} else if(jsonMapping.containsKey(SchemaConstants.VIEW_QUERY_KEY.getLabel())) {
				nativeQuery = jsonMapping.getString(SchemaConstants.VIEW_QUERY_KEY.getLabel());
			} else {
				throw new ViewParseException("No query associated to view " + id);
			}

			String position = null;
			if(jsonMapping.containsKey(SchemaConstants.VIEW_POSITION_KEY.getLabel())) {
				position = jsonMapping.getString(SchemaConstants.VIEW_POSITION_KEY.getLabel());
			}


			// Create templates map
			List<ViewSignature> templates = new ArrayList<>();

			JsonArray jsonTemplates = jsonMapping.getJsonArray(SchemaConstants.VIEW_SIGNATURE_KEY.getLabel());
			for(JsonValue jsonTemplate : jsonTemplates) {
				JsonObject jsonObjectTemplate = jsonTemplate.asJsonObject();
				String templateMandatory = null;
				if(jsonObjectTemplate.containsKey(SchemaConstants.VIEW_SIGNATURE_MANDATORY_KEY.getLabel())) {
					templateMandatory = jsonObjectTemplate.getString(SchemaConstants.VIEW_SIGNATURE_MANDATORY_KEY.getLabel());
				}
				String templateIfMissing = IF_MISSING_DEFAULT_VALUE;
				if(jsonObjectTemplate.containsKey(SchemaConstants.VIEW_SIGNATURE_IFMISSING_KEY.getLabel())) {
					templateIfMissing = jsonObjectTemplate.getString(SchemaConstants.VIEW_SIGNATURE_IFMISSING_KEY.getLabel());
					if(! ViewParser.IF_MISSING_SUPPORTED_VALUES.contains(templateIfMissing)) {
						LOG.warn("The ifMissing value {} is not part of the predefined ones : {}.", templateIfMissing, ViewParser.IF_MISSING_SUPPORTED_VALUES);
					}
				}
				String templateSelection = null;
				if(jsonObjectTemplate.containsKey(SchemaConstants.VIEW_SIGNATURE_SELECTION_KEY.getLabel())) {
					templateSelection = jsonObjectTemplate.getString(SchemaConstants.VIEW_SIGNATURE_SELECTION_KEY.getLabel());
				}
				templates.add(new ViewSignature(templateMandatory, templateIfMissing, templateSelection));
			}

			mappings.add(new ViewDefinition(id, datasourceId, nativeQuery, position, templates));
		}

		return mappings;
	}

	private static JsonNode getJsonNodeFromFilePath(String filepath) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(new File(filepath));
	}

	private static JsonSchema getJsonSchemaFromUrl(String uri) throws URISyntaxException {
		JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);
		return factory.getSchema(ViewParser.class.getResource(uri).toURI());
	}

}
