package fr.inria.integraal.view_parser.parser;

import java.util.List;
import java.util.Optional;

/**
 * This class represents a mapping of the mapping specification format
 * @author Florent Tornil
 */
public class ViewDefinition {
	
	private final String id;
	private final String datasourceId;
	private final String nativeQuery;
	private final Optional<String> position;
	
	private final List<ViewSignature> templates;

	/**
	 * Create a new mapping with the given parameters
	 * @param id the id of the mapping (ie: the name of the view)
	 * @param datasourceId the id of the datasource linked with the mapping
	 * @param nativeQuery the native query for the mapping
	 * @param position the JsonPath positioning query in case of a JSON datasource
	 * @param templates the templates for the mapping
	 */
	public ViewDefinition(String id, String datasourceId, String nativeQuery, String position, List<ViewSignature> templates) {
		this.id = id;
		this.datasourceId = datasourceId;
		this.nativeQuery = nativeQuery;
		this.position = Optional.ofNullable(position);
		this.templates = templates;
	}

	/**
	 * Getter for the view ID
	 * @return the id of this mapping
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Getter for the view datasource
	 * @return the id of the datasource on which this mapping applies
	 */
	public String getDatasourceId() {
		return this.datasourceId;
	}

	/**
	 * Getter for the view native query
	 * @return the native query associated with this mapping
	 */
	public String getNativeQuery() {
		return this.nativeQuery;
	}
	
	/**
	 * Getter for the view positioning Jsonpath query
	 * @return the native query associated with this mapping
	 */
	public Optional<String> getPositioning() {
		return this.position;
	}

	/**
	 * Getter for the view signature
	 * @return the templates of this mapping
	 */
	public List<ViewSignature> getTemplates() {
		return this.templates;
	}
	
	//
	//
	//
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[View Definition]\n");
		sb.append("      Id : " + this.getId() + "\n");
		sb.append("      Datasource : " + this.getDatasourceId() + "\n");
		sb.append("      Query : " + this.getNativeQuery() + "\n");
		if(this.getPositioning().isPresent()) {
			sb.append("      Position : " + this.getPositioning().get() + "\n");
		}
		sb.append("      Signature : [");
		for(ViewSignature template : this.getTemplates()) {
			sb.append("\n");
			sb.append("         " + template);
		}
		sb.append("\n      ]");
		return sb.toString();
	}
	

}
