package fr.inria.integraal.view_parser.parser;

import java.util.List;
import java.util.Optional;

/**
 * This class represents the type of datasource
 * @author Florent Tornil
 *
 */
public enum DatasourceType {
	/**
	 * POSTGRESQL type
	 */
	POSTGRESQL("PostgreSQL", "SQL",
			List.of("url", "database"),
			List.of("user", "password", "passwordFile")),
	/**
	 * SQLite type
	 */
	SQLITE("SQLite", "SQL",
			List.of("url"),
			List.of()),
	/**
	 * HSQLDB type
	 */
	HSQLDB("HSQLDB", "SQL",
			List.of("url", "database"),
			List.of("user", "password", "passwordFile")),
	/**
	 * MySQL type
	 */
	MYSQL("MySQL", "SQL",
			List.of("url", "database"),
			List.of("user", "password", "passwordFile")),
	/**
	 * MONGODB type
	 */
	MONGODB("MongoDB", "MongoAggregationQuery",
			List.of("url", "database"),
			List.of("user", "password", "passwordFile")),

	/**
	 * SPARQL endpoint type
	 */
	SPARQLENDPOINT("SparqlEndpoint", "SPARQL",
			List.of("url"),
			List.of()),
	/**
	 * WEB API type
	 */
	WEBAPI("JSONWebApi", "HTTPQuery",
			List.of(),
			List.of("username", "password", "passwordFile"));

	private final String label;
	private final String defaultQueryType;
	private final List<String> requiredParameters;
	private final List<String> optionalParameters;

	DatasourceType(String label, String defaultQueryType, List<String> requiredParameters, List<String> optionalParameters) {
		this.label = label;
		this.defaultQueryType = defaultQueryType;
		this.requiredParameters = requiredParameters;
		this.optionalParameters = optionalParameters;
	}
	
	/**
	 * Return the correct DatasourceType according to a String label
	 * @param label the label of the datasource type
	 * @return the element of the enumeration that correspond to the given label or an empty optional if there is none
	 */
	public static Optional<DatasourceType> getTypeByLabel(String label) {
		if(label.equals(POSTGRESQL.getLabel())) {
			return Optional.of(POSTGRESQL);
		} else if(label.equals(SQLITE.getLabel())) {
			return Optional.of(SQLITE);
		} else if(label.equals(HSQLDB.getLabel())) {
			return Optional.of(HSQLDB);
		} else if(label.equals(MYSQL.getLabel())) {
			return Optional.of(MYSQL);
		} else if(label.equals(MONGODB.getLabel())) {
			return Optional.of(MONGODB);
		} else if(label.equals(SPARQLENDPOINT.getLabel())) {
			return Optional.of(SPARQLENDPOINT);
		} else if(label.equals(WEBAPI.getLabel())) {
			return Optional.of(WEBAPI);
		} else {
			return Optional.empty();
		}
	}

	//
	//
	//
	
	/**
	 * Getter for datasource type label
	 * @return the label for this type
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Getter for datasource default query type
	 * @return the default type of the native query for this type
	 */
	public String getDefaultQueryType() {
		return defaultQueryType;
	}

	/**
	 * Getter for datasource required parameters
	 * @return the list of required parameters for this type
	 */
	public List<String> getRequiredParameters() {
		return requiredParameters;
	}

	/**
	 * Getter for datasource optional parameters
	 * @return the list of optional parameters for this type
	 */
	public List<String> getOptionalParameters() {
		return optionalParameters;
	}


}
