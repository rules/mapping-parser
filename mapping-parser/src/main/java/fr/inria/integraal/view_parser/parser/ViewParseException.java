package fr.inria.integraal.view_parser.parser;

/**
 * Exception for mapping parser
 */
public class ViewParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9013709938740015248L;
	
	/**
	 * Construct a new exception with the given message
	 * @param message the description of the exception
	 */
	public ViewParseException(String message) {
		super(message);
	}

}
