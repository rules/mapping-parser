package fr.inria.integraal.view_parser.parser;

/**
 * Constants used for the schema keys
 * 
 * @author Florent Tornil
 *
 */
public enum SchemaConstants {

	/**
	 * Key for the datasources entry
	 */
	DATASOURCES_KEY("datasources"),

	/**
	 * Key for a datasource id
	 */
	DATASOURCE_ID_KEY("id"),

	/**
	 * Key for a datasource protocol
	 */
	DATASOURCE_PROTOCOL_KEY("protocol"),

	/**
	 * Key for a datasource parameters entry
	 */
	DATASOURCE_PARAMETERS_KEY("parameters"),

	/**
	 * Key for a datasource parameter password
	 */
	DATASOURCE_PARAMETERS_PASSWORD_KEY("password"),

	/**
	 * Key for a datasource parameter password stored in a file
	 */
	DATASOURCE_PARAMETERS_PASSWORD_FILE_KEY("passwordFile"),



	/**
	 * Key for the views entry
	 */
	VIEWS_KEY("views"),

	/**
	 * Key for a view id
	 */
	VIEW_ID_KEY("id"),

	/**
	 * Key for a view datasource
	 */
	VIEW_DATASOURCE_KEY("datasource"),

	/**
	 * Key for a view query
	 */
	VIEW_QUERY_KEY("query"),

	/**
	 * Key for a view that returns a JSON document positioning
	 */
	VIEW_POSITION_KEY("position"),

	/**
	 * Key for a view query stored in a file
	 */
	VIEW_QUERY_FILE_KEY("queryFile"),

	/**
	 * Key for a view signature entry
	 */
	VIEW_SIGNATURE_KEY("signature"),

	/**
	 * Key for a view signature mandatory replacement key
	 */
	VIEW_SIGNATURE_MANDATORY_KEY("mandatory"),

	/**
	 * Key for a view signature missing value strategy
	 */
	VIEW_SIGNATURE_IFMISSING_KEY("ifMissing"),

	/**
	 * Key for a view signature JSON value selection
	 */
	VIEW_SIGNATURE_SELECTION_KEY("selection");

	private final String label;

	SchemaConstants(String label) {
		this.label = label;
	}

	/**
	 * Getter for the value of SchemaConstants
	 * @return the value of the SchemaConstant
	 */
	public String getLabel() {
		return this.label;
	}



}
